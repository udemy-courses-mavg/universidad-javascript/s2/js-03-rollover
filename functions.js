window.onload = loadImages;

function loadImages() {
  console.log(document.images.length);
  for (var i=0; i<document.images.length; i++) {
    if (document.images[i].parentNode.tagName == "A")
      configureRollover(document.images[i])
  }
}

function configureRollover(image) {
  image.imageOff = new Image();
  image.imageOff.src = "images/boton_off.jpg";
  image.onmouseout = changeOff;

  image.imageOn = new Image();
  image.imageOn.src = "images/boton_on.jpg";
  image.onmouseover = changeOn;
}

/**
 * These functions are execute according the event fired (activated) 
 * Depends of an action, like push a button, for example.
 * Are known such as Handlers
 */
// onmouseout
function changeOff() {
  this.src = this.imageOff.src;
}

// onmouseover
function changeOn() {
  this.src = this.imageOn.src;
}
